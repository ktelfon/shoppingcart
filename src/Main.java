import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Product[] cart = new Product[5];

        Scanner sc = new Scanner(System.in);
        int i = 0;
        while (i < cart.length) {

            System.out.println("Add or Leave ?");
            String input = sc.nextLine();
            if(input.toLowerCase().equals("leave")){
                break;
            }

            System.out.println("Enter product name");
            String name = sc.nextLine();
            System.out.println("Enter price");
            double price = sc.nextDouble();
            sc.nextLine();
            cart[i] = new Product(name, price);
            i++;
        }

        int sum = 0;
        for (Product product : cart) {
            sum += product.getPrice();
        }

        System.out.println("Total: " + sum);


    }
}
